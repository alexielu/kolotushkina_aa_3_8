﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ziggurat
{
    public class GameManager : MonoBehaviour
    {
        [Header("Game Elements")]
        [SerializeField, Tooltip("Спавнящие юнитов ворота")] private List<ZigguratGate> _ziggurats;
        [SerializeField, Tooltip("Префаб юнита")] private GameObject _unitPrefab;
        [SerializeField, Tooltip("Точка в центре сцены, к которой начинают движение юниты")] private Transform _anchor;
        [SerializeField, Tooltip("Вспомогательный префаб точки для блуждания")] private GameObject _wanderPoint;
        
        [Header("Common Movement Restrictions")]
        [SerializeField, Tooltip("Квадрат расстояния до центра сцены, на котором начинается поиск врагов")]
        private float _sqrSqrHuntingDistance = 1225f;
        [SerializeField, Tooltip("Квадрат расстояния остановки перед врагом")]
        private float _sqrArrivalDistance = 2.25f;
        [SerializeField, Tooltip("Время задержки между атаками")]
        private float _attackDelay = 2f; 
       
        public List<ZigguratGate> Ziggurats => _ziggurats;
        public GameObject UnitPrefab => _unitPrefab;
        public Transform Anchor => _anchor;

        public float SqrHuntingDistance => _sqrSqrHuntingDistance;
        public float SqrArrivalDistance => _sqrArrivalDistance;
        public float AttackDelay => _attackDelay;

        public static GameManager Instance;

        private void Awake()
        {
            if (Instance != null)
                Destroy(this);
            else
                Instance = this;
        }
    }
}
