﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Ziggurat
{
    // Класс, производящий расчеты для юнитов
    public class UnitsAssistant 
    {
        // Находит ближайшего врага
        public UnitController GetClosestEnemy(ZigguratColor hunterColor, Vector3 hunterPosition)
        {
            List<UnitController> units = new List<UnitController>();

            var ziggs = GameManager.Instance.Ziggurats;
            foreach (var zigg in ziggs)
            {
                if (zigg.ZiggColor == hunterColor) continue;

                var armyToAdd = zigg.Army.Where(t =>
                        Mathf.Abs(t.transform.position.x) < 35f && Mathf.Abs(t.transform.position.z) < 33f);
                units.AddRange(armyToAdd);
            }

            var minDistance = Mathf.Infinity;
            UnitController unitToReturn = null;

            foreach (var unit in units)
            {
                if (Vector3.Distance(unit.transform.position, hunterPosition) < minDistance)
                {
                    minDistance = Vector3.Distance(unit.transform.position, hunterPosition);
                    unitToReturn = unit;
                }
            }

            return unitToReturn;
        }
    }
}
