﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Ziggurat
{
    public class ZigguratGate : MonoBehaviour, IPointerClickHandler
    {
        // Событие нажатия на зиккурат
        public delegate void ClickEventHandler(ZigguratGate ziggurat);
        public event ClickEventHandler OnClickEventHandler;
        
        [SerializeField] private ZigguratColor _ziggColor;
        [SerializeField] private Material _material;
        [SerializeField] private StatisticsPanel _statsPanel; // Соответствующая зиккурату панель с параметрами
        [SerializeField] private Vector3 _localSpawnPoint; // Точка спавна относительно расположения зиккурата
        [SerializeField] private Transform _unitsHolder;

        [Header("Parameters")]
        [SerializeField, Tooltip("Таймер спавна"), Range(.1f, 10f)] private float _spawnTimer = 1f;
        private float _currentTimer;
        [SerializeField, Tooltip("Параметры юнитов")] private UnitSetup _unitSetup;

        public ZigguratColor ZiggColor => _ziggColor;
        public List<UnitController> Army => _units;
        public float SpawnTimer
        {
            get => _spawnTimer;
            set => _spawnTimer = value;
        }
        public UnitSetup Setup
        {
            get => _unitSetup;
            set => _unitSetup = value;
        }
        
        private List<UnitController> _units = new List<UnitController>();

        void Start()
        {
            _currentTimer = 1000f;
            SpawnUnit();
        }

        void Update()
        {
            // Расчет спавна
            if (_currentTimer > 0)
            {
                if (_currentTimer > _spawnTimer)
                    _currentTimer = _spawnTimer;
                
                _currentTimer -= Time.deltaTime;
                _statsPanel.UpdateTimer(_currentTimer);
            }
            else
                SpawnUnit();
        }
        
        // Создание юнита с актуальными параметрами
        private void SpawnUnit()
        {
            var rot = Quaternion.LookRotation(GameManager.Instance.Anchor.position - transform.position, Vector3.up);
            var unit = Instantiate(GameManager.Instance.UnitPrefab, transform.position + _localSpawnPoint, rot, _unitsHolder);
            
            var unitControl = unit.GetComponent<UnitController>();
            
            _units.Add(unitControl);
            _statsPanel.UpdateAlive(_units.Count);
            unitControl.SetParams(this, _unitSetup, _material);

            _currentTimer = SpawnTimer;
        }

        // Удаление юнита из "армии"
        public void RemoveUnit(UnitController unit)
        {
            if (_units.Contains(unit))
            {
                _units.Remove(unit);
                _statsPanel.UpdateAliveAndDead(_units.Count);
            }
        }
        
        // Удаление всей "армии"
        public void KillAll()
        {
            for (int i = _units.Count - 1; i >= 0; i--)
            {
                var u = _units[i];
                RemoveUnit(u);
                Destroy(u.gameObject);
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnClickEventHandler?.Invoke(this);
        }
    }
}
