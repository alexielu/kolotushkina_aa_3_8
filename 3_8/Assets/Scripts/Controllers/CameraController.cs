﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Ziggurat
{
    // Управление камерой
    public class CameraController : MonoBehaviour
    {
        private CameraControls _controls;

        [SerializeField] private float _movementSpeed;
        [SerializeField] private float _rotationSpeed;
        [SerializeField] private float _scaleSpeed;

        private bool _allowedRotation;
        private float _xRot = 0f;
        private float _yRot = 0f;

        void Start()
        {
            _controls = new CameraControls();
            _controls.Enable();

            _xRot = transform.eulerAngles.x;
            _yRot = transform.eulerAngles.y;

            _controls.Camera.RotationActivation.started += _ => { _allowedRotation = true; };
            _controls.Camera.RotationActivation.canceled += _ => { _allowedRotation = false; };
        }

        void Update()
        {
            // Movement
            var moveValue = _controls.Camera.Movement.ReadValue<Vector2>();
            var direction = transform.right * moveValue.x +
                            new Vector3(transform.forward.x, 0, transform.forward.z) * moveValue.y;

            transform.position += direction.normalized * _movementSpeed * Time.deltaTime;

            // Rotation
            if (_allowedRotation)
            {
                var rotValue = _controls.Camera.Rotation.ReadValue<Vector2>();
                _xRot -= _rotationSpeed * rotValue.y;
                _yRot += _rotationSpeed * rotValue.x;

                transform.rotation = Quaternion.Euler(new Vector3(_xRot, _yRot, 0f));
            }

            // Scale
            var scaleValue = _controls.Camera.Scale.ReadValue<Vector2>();
            transform.position += transform.forward * scaleValue.y * _scaleSpeed * Time.deltaTime;
        }
    }
}
