﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ziggurat
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] private Slider _slider;

        private int _maxValue;
        private int _currentValue;

        void Update()
        {
            // Поворот интерфейса в сторону камеры
            transform.LookAt(Camera.main.transform);
        }

        // Включение / Выключение отображения шкалы здоровья
        public void ShowBar(bool on)
        {
            _slider.gameObject.SetActive(on);

            if (on)
            {
                _slider.value = _currentValue;

                if (_slider.maxValue != _maxValue)
                    _slider.maxValue = _maxValue; 
            }
        }

        // Установка изначальных параметров шкалы 
        public void SetBar(int maxHealth)
        {
            _maxValue = maxHealth;
            _currentValue = maxHealth;
            
            if (_slider.isActiveAndEnabled)
            {
                _slider.maxValue = maxHealth;
                _slider.value = maxHealth;
            }
        }

        // Обновление текущего показателя здоровья
        public void UpdateBar(int health)
        {
            _currentValue = health;
            
            if (_slider.isActiveAndEnabled)
                _slider.value = health;
        }
    }
}