﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ziggurat
{
    public class UIController : MonoBehaviour
    {
        [Header("Panels")] // Игровые окна
        [SerializeField] private StatisticsWindow _statistics;
        [SerializeField] private StatsWindow _stats;

        private List<ZigguratGate> _ziggurats;
        private bool _barsOn; // Включены ли шкалы здоровья
        
        public static UIController Instance;
        
        void Start()
        {
            _barsOn = true;
            _ziggurats = GameManager.Instance.Ziggurats;
            foreach (var zigg in _ziggurats)
            {
                zigg.OnClickEventHandler += OnZigguratClick;
            }
        }

        // Открытие окна по клику на зиккурат
        private void OnZigguratClick(ZigguratGate zigguratGate)
        {
            _stats.OpenWindow(zigguratGate);
        }
        
        // Уничтожение всех юнитов в сцене
        public void KillAll()
        {
            foreach (var zigg in _ziggurats)
            {
                zigg.KillAll();
            }
        }

        // Показать / Спрятать шкалы здоровья
        public void ShowHealthBars()
        {
            _barsOn = !_barsOn;
            
            foreach (var zigg in _ziggurats)
            {
                var units = zigg.Army;
                foreach (var unit in units)
                {
                    unit.transform.GetComponentInChildren<HealthBar>().ShowBar(_barsOn);
                }
            }
        }
    }
}
