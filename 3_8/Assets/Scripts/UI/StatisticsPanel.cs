﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Ziggurat
{
    public class StatisticsPanel : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _alive; // Кол-во живых юнитов
        [SerializeField] private TextMeshProUGUI _dead; // Кол-во убитых
        [SerializeField] private TextMeshProUGUI _timer; // Остаток времени до спавна

        private int _deadAmount;

        void OnEnable()
        {
            _deadAmount = -1;
            UpdateAliveAndDead(0);
        }

        // Методы корректировки значений в таблице
        public void UpdateAliveAndDead(int aliveUnits)
        {
            UpdateAlive(aliveUnits);

            _deadAmount++;
            _dead.text = "Dead: " + _deadAmount;
        }

        public void UpdateAlive(int aliveUnits)
        {
            _alive.text = "Alive: " + aliveUnits;
        }

        public void UpdateTimer(float time)
        {
            _timer.text = time > 0 ? time.ToString("00.0") : "0";
        }
        
        public void ResetData()
        {
            _deadAmount = 0;
            _dead.text = "Dead: " + _deadAmount;
        }
    }
}
