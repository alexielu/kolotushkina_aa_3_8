﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Ziggurat;

namespace Ziggurat
{
    public class StatisticsWindow : MonoBehaviour
    {
        // Параметры для анимации
        [SerializeField] private RectTransform _buttonIcon;
        [SerializeField] private float _closedYPos;
        [SerializeField] private float _openedYPos;
        [SerializeField] private float _animationTimer;

        // Дочерние панели со статистикой по каждому зиккурату
        private List<StatisticsPanel> _panels = new List<StatisticsPanel>();

        void Start()
        {
            _panels = transform.GetComponentsInChildren<StatisticsPanel>().ToList();
        }

        // Запуск анимации
        public void ActivateAnimation()
        {
            StartCoroutine(MovePanel());
        }

        // Сброс параметров
        public void ResetStatistics()
        {
            foreach (var panel in _panels)
            {
                panel.ResetData();
            }
        }

        // Анимация открытия и закрытия окна
        private IEnumerator MovePanel()
        {
            // Отключение кнопок
            var buttons = transform.GetComponentsInChildren<Button>(true);
            foreach (var b in buttons)
            {
                b.interactable = false;
            }

            var time = 0f;
            var tr = GetComponent<RectTransform>();
            bool opening = Mathf.Abs(tr.anchoredPosition.y - _closedYPos) < 1;

            //todo if opening -> close all opened windows

            while (time < _animationTimer)
            {
                tr.anchoredPosition = new Vector3(0, opening
                    ? Mathf.Lerp(_closedYPos, _openedYPos, time / _animationTimer)
                    : Mathf.Lerp(_openedYPos, _closedYPos, time / _animationTimer), 0);

                _buttonIcon.localRotation =
                    Quaternion.Euler(0, 0, opening
                        ? Mathf.Lerp(-90f, 90f, time / _animationTimer)
                        : Mathf.Lerp(90f, -90f, time / _animationTimer));

                time += Time.deltaTime;
                yield return null;
            }

            tr.anchoredPosition = new Vector3(0, opening ? _openedYPos : _closedYPos, 0);
            _buttonIcon.localRotation = Quaternion.Euler(0, 0, opening ? 90f : -90f);

            foreach (var b in buttons)
            {
                b.interactable = true;
            }

            yield return null;
        }
    }
}
