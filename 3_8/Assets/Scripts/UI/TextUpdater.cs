﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ziggurat
{
    // Вспомогательный класс для вывода значения у слайдера
    public class TextUpdater : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _text;
        private Slider _slider;

        void Start()
        {
            _slider = GetComponent<Slider>();
        }

        public void UpdateText()
        {
            _text.text = _slider.value.ToString("0.00");
        }
    }
}
