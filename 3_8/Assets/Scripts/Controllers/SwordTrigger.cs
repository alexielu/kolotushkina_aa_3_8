﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ziggurat
{
    public class SwordTrigger : MonoBehaviour
    {
        // Фиксация нанесения урона
        private void OnTriggerEnter(Collider other)
        {
            var enemy = other.GetComponentInParent<UnitController>();
            if (enemy != null)
                GetComponentInParent<UnitController>().SetDamageToEnemy(enemy);
        }
    }
}
