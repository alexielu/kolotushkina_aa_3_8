﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Ziggurat
{
    public class UnitController : MonoBehaviour
    {
        [SerializeField] private HealthBar _healthBar; // Шкала здоровья
        [SerializeField] private MeshRenderer _renderToColor; // Элемент, который красится в цвет зиккурата
        private Rigidbody _rigidBody;

        private UnitsAssistant _assistant; // Класс вспомогательных рассчетов
        private UnitEnvironment _unitEnv;

        private ZigguratGate _myZiggurat;
        private UnitSetup _setup; // Набор настроек юнита

        private float _sqrMinDistance; // квадрат дистанции остановки
        private float _sqrHuntingDistance; // квадрат дистанции начала поиска врагов
        private float _attackDelay; // задержка между атаками

        private int _currentDmg; // текущий расчет урона (учет есть ли крит, слабая или сильная атака)
        private Coroutine _attackCoroutine;

        public bool InBattle { get; set; } // Сражается ли юнит
        public Transform Target { get; set; } // Цель юнита

        void Start()
        {
            // Фиксирование всех необходимых полей 
            
            _assistant = new UnitsAssistant();
            _rigidBody = GetComponent<Rigidbody>();
            _unitEnv = GetComponent<UnitEnvironment>();
            Target = GameManager.Instance.Anchor;

            _sqrMinDistance = GameManager.Instance.SqrArrivalDistance;
            _sqrHuntingDistance = GameManager.Instance.SqrHuntingDistance;
            _attackDelay = GameManager.Instance.AttackDelay;

            _unitEnv.OnEndAnimation += OnAnimEnd;

            StartCoroutine(SeekMovement());
        }

        // Окончание анимации смерти
        void OnAnimEnd(object sender, EventArgs eventArgs)
        {
            _myZiggurat.RemoveUnit(this);
        }

        // Получение параметров от зиккурата
        public void SetParams(ZigguratGate ziggurat, UnitSetup setup, Material material)
        {
            _myZiggurat = ziggurat;
            _setup = setup;
            _healthBar.SetBar(_setup.health);
            ColorMesh(material);
        }

        // Поиск новой цели
        private void UpdateTarget()
        {
            var enemy = _assistant.GetClosestEnemy(_myZiggurat.ZiggColor, transform.position);

            if (enemy == null)
                Debug.Log("No Enemies Around");
            else
                Target = enemy.transform;
        }

        // Корутина перемещения юнита
        private IEnumerator SeekMovement()
        {
            while (true)
            {
                if (Target == null) // Есть ли куда стремиться
                {
                    UpdateTarget();
                    if (Target == null) // Врагов поблизости нет, выжидаем
                    {
                        yield return null;
                        continue;
                    }
                }

                // Расчет направления
                var targetPosition = Target.position;
                targetPosition.y = transform.position.y;
                var desiredVelocity = (targetPosition - transform.position).normalized * _setup.speed * 3f;
                var sqrLength = (targetPosition - transform.position).sqrMagnitude;
                
                if (sqrLength < _sqrHuntingDistance && Target.GetComponent<UnitController>() == null) // Момент попадания в зону охоты
                {
                    UpdateTarget(); // Ищем врагов
                }
                else if (sqrLength < _sqrMinDistance) // Подошел к врагу
                {
                    SetVelocity(Vector3.zero);
                    _unitEnv.Moving(0f);

                    if (_attackCoroutine == null) // Запуск поведения атаки
                    {
                        InBattle = true;
                        _attackCoroutine = StartCoroutine(Battle());
                    }
                }
                else // Продолжение движения
                {
                    if (InBattle)
                    {
                        InBattle = false;
                        _attackCoroutine = null;
                    }

                    var steering = desiredVelocity - GetVelocity(IgnoreAxisType.Y);
                    steering = Vector3.ClampMagnitude(steering, _setup.speed) / _rigidBody.mass;

                    var velocity = Vector3.ClampMagnitude(GetVelocity() + steering, _setup.speed);
                    SetVelocity(velocity, IgnoreAxisType.Y);
                }

                yield return null;
            }
        }

        // Поведение в битве
        private IEnumerator Battle()
        {
            while (InBattle)
            {
                if (Random.Range(0f, 1f) > _setup.missChance) // Проверка на промах
                {
                    // Проверка типа атаки
                    if (Random.Range(0f, 1f) < _setup.fastToSlowRatio)
                    {
                        _unitEnv.StartAnimation("Fast");
                        _currentDmg = _setup.fastAttackDamage;
                    }
                    else
                    {
                        _unitEnv.StartAnimation("Strong");
                        _currentDmg = _setup.slowAttackDamage;
                    }

                    // Расчет финального размера урона
                    bool crit = Random.Range(0f, 1f) > _setup.critChance;
                    if (crit) _currentDmg *= 2;
                }

                yield return new WaitForSeconds(_attackDelay);
            }
        }

        // Нанесение урона
        public void SetDamageToEnemy(UnitController enemy)
        {
            enemy.GetDamage(_currentDmg);
        }

        // Получение урона
        public void GetDamage(int damage)
        {
            _setup.health -= damage;

            if (_setup.health <= 0)
            {
                _healthBar.UpdateBar(0);
                _unitEnv.StartAnimation("Die");
                return;
            }

            _healthBar.UpdateBar(_setup.health);
        }

        // Покраска щита
        private void ColorMesh(Material mat)
        {
            _renderToColor.material = mat;
        }

        private Vector3 IgnoreAxisUpdate(IgnoreAxisType ignore, Vector3 velocity)
        {
            if (ignore == IgnoreAxisType.None) return velocity;
            if ((ignore & IgnoreAxisType.X) == IgnoreAxisType.X) velocity.x = 0f;
            if ((ignore & IgnoreAxisType.Y) == IgnoreAxisType.Y) velocity.y = 0f;
            if ((ignore & IgnoreAxisType.Z) == IgnoreAxisType.Z) velocity.z = 0f;

            return velocity;
        }

        public Vector3 GetVelocity(IgnoreAxisType ignore = IgnoreAxisType.Y)
        {
            return IgnoreAxisUpdate(ignore, _rigidBody.velocity);
        }

        private void SetVelocity(Vector3 velocity, IgnoreAxisType ignore = IgnoreAxisType.None)
        {
            _unitEnv.Moving(1f);

            transform.LookAt(new Vector3(Target.position.x, transform.position.y, Target.position.z));

            var velocityToSet = IgnoreAxisUpdate(ignore, velocity);
            _rigidBody.velocity = new Vector3(velocityToSet.x, _rigidBody.velocity.y, velocityToSet.z);
        }
    }
}
