﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Slider = UnityEngine.UI.Slider;

namespace Ziggurat
{
    public class StatsWindow : MonoBehaviour
    {
        private bool _closed; // текущее состояние окна
        private bool _playerEditing; // меняются ли данные игроком
        
        [Header("Animation Parameters")]
        [SerializeField] private float _closedXPos;
        [SerializeField] private float _openedXPos;
        [SerializeField] private float _animationTimer;

        [SerializeField] private ZigguratGate _currentZiggurat;
        
        [Header("Stats Elements")] // Поля с параметрами юнитов
        [SerializeField] private TextMeshProUGUI _zigguratType;
        [SerializeField] private Slider _spawnSlider;
        [SerializeField] private TMP_InputField _healthInput;
        [SerializeField] private TMP_InputField _speedInput;
        [SerializeField] private TMP_InputField _fastAttackInput;
        [SerializeField] private TMP_InputField _slowAttackInput;
        [SerializeField] private Slider _missSlider;
        [SerializeField] private Slider _critSlider;
        [SerializeField] private Slider _ratioSlider;

        void Start()
        {
            _closed = Mathf.Abs(GetComponent<RectTransform>().anchoredPosition.x - _closedXPos) < 1;
        }
        
        // Открытие окна
        public void OpenWindow(ZigguratGate ziggurat)
        {
            SetParams(ziggurat);
            
            if (_closed)
                StartCoroutine(MovePanel());
        }
        
        // Выставление изначальных параметров
        public void SetParams(ZigguratGate ziggurat)
        {
            _playerEditing = false;
            
            _currentZiggurat = ziggurat;
            _zigguratType.text = _currentZiggurat.ZiggColor.ToString();
            _spawnSlider.value = _currentZiggurat.SpawnTimer;

            var setup = _currentZiggurat.Setup;
            _healthInput.text = setup.health.ToString();
            _speedInput.text = setup.speed.ToString();
            _fastAttackInput.text = setup.fastAttackDamage.ToString();
            _slowAttackInput.text = setup.slowAttackDamage.ToString();
            _missSlider.value = setup.missChance;
            _critSlider.value = setup.critChance;
            _ratioSlider.value = setup.fastToSlowRatio;

            _playerEditing = true;
        }

        // Фиксация изменения параметров
        public void OnValueUpdated()
        {
            if (!_playerEditing) return;
            
            _currentZiggurat.SpawnTimer = (float)Math.Round(_spawnSlider.value, 2);
            
            UnitSetup newSetup = new UnitSetup();

            int.TryParse(_healthInput.text, out newSetup.health);
            float.TryParse(_speedInput.text, out newSetup.speed);
            int.TryParse(_fastAttackInput.text, out newSetup.fastAttackDamage);
            int.TryParse(_slowAttackInput.text, out newSetup.slowAttackDamage);
            newSetup.missChance = (float)Math.Round(_missSlider.value, 2);
            newSetup.critChance = (float)Math.Round(_critSlider.value, 2);
            newSetup.fastToSlowRatio = (float)Math.Round(_ratioSlider.value, 2);
            
            _currentZiggurat.Setup = newSetup;
        }

        public void HideWindow()
        {
            StartCoroutine(MovePanel());
        }

        // Включение и выключение интерактивных элементов
        private void ActivateElements(bool on)
        {
            var elements = transform.GetComponentsInChildren<Selectable>();
            foreach (var e in elements)
            {
                e.interactable = on;
            }
        }
        
        // Анимация окна
        private IEnumerator MovePanel()
        {
            ActivateElements(false);

            var time = 0f;
            var tr = GetComponent<RectTransform>();

            while (time < _animationTimer)
            {
                tr.anchoredPosition = new Vector3(_closed 
                    ? Mathf.Lerp(_closedXPos, _openedXPos, time/_animationTimer) 
                    : Mathf.Lerp(_openedXPos, _closedXPos, time/_animationTimer),0, 0);

                time += Time.deltaTime;
                yield return null;
            }

            tr.anchoredPosition = new Vector3(_closed ? _openedXPos : _closedXPos,0,0);
            ActivateElements(true);
            _closed = !_closed;
            yield return null;
        }
    }
}
