﻿using System;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Ziggurat
{
	[Serializable]
	public struct UnitSetup
	{
		[SerializeField, Tooltip("Здоровье юнита")]
		public int health;

		[SerializeField, Tooltip("Скорость перемещения"), Range(.1f, 20f)]
		public float speed;

		[SerializeField, Tooltip("Урон от быстрой атаки")]
		public int fastAttackDamage;

		[SerializeField, Tooltip("Урон от медленной атаки")]
		public int slowAttackDamage;

		[SerializeField, Tooltip("Вероятность промаха"), Range(0f, 1f)]
		public float missChance;

		[SerializeField, Tooltip("Вероятность крита"), Range(0f, 1f)]
		public float critChance;

		[SerializeField, Tooltip("Процентное соотношение вероятности быстрой атаки к медленной"), Range(0f, 1f)]
		public float fastToSlowRatio;
	}
	
	public enum ZigguratColor : byte
	{
		Cyan = 0,
		Magenta = 1,
		Yellow = 2
	}

	public enum AnimationType : byte
	{
		Move = 0,
		FastAttack = 1,
		SlowAttack = 2,
		Die = 3
	}

	[System.Flags]
	public enum IgnoreAxisType : byte
	{
		None = 0,
		X = 1,
		Y = 2,
		Z = 4
	}

	[System.Serializable]
	public class AnimationKeyDictionary : SerializableDictionaryBase<AnimationType, string> { }
}
